# lungfish

Please refer to this page [from here](#issues-with-the-previous-analysis) to see the newest results.

## TODO list

TODO list is managed by the issues of gitlab

## materials and methods

### Transcriptomes and genomes
the following transcriptomes (or genomes where available) were used:
+ Lepisosteus oculatus
+ Mondelphis domestica
+ Lepidosiren paradoxa 
+ Protopterus aethiopicus
+ Anguilla anguilla
+ Neoceratodus forsteri
+ Gallus gallus
+ Protopterus annectens
+ Anolis carolinensis
+ Danio rerio
+ Protopterus dolloi
+ Cynops orientalis
+ Callorhinchus milii 
+ Homo Sapiens
+ Ambystoma mexicanum
### othology search
BUSCO v4.1.4 was used against the vertebrata database from orthodb v10.   
In the latest update buscov5.0 was used with the same database as before. 

Sample code:
```
busco -i ../Protopterus/Protopterus_dolloi.trinity.fasta -l vertebrata -o Protopterus_dolloi_busco_vertebrata -m tran -c 120 -f
```

Output of busco was extracted with:
```bash
for file in `find . -maxdepth 1 -type d -name "*vertebrata"`
do
cp $file/run_vertebrata_odb10/full_table.tsv  $(basename $file).busco.tsv
done

```

Selecting complete and duplicated buscos:
```bash
for file in `find . -maxdepth 1 -mindepth 1 -name "*.tsv"`
do
grep -e "\(Complete\|Duplicated\)" $file | cut -f1 | sort | uniq > $file.complete_buscos.list
done
```

Count common buscos:
```bash
cat *.uniq_buscos | sort | uniq -c | sort -g -k 1 > common_buscos_ranked_by_counts.tbl

```
[see results](#selection-of-candidate-orthologs)

#### Adding the genome for *Neoceratodus forsteri*
CDSs were extracted from the annotated genome and merged with the CDS derived from the TransDecoder of the transcriptome to achieve completeness (the genome is fairly incomplete)


### Extraction of sequences

Filter based on presence in at least 15 species (change number as you wish):
```bash
awk '{if ($1 > 14) { print }}' common_buscos_ranked_by_counts.tbl

## filtering and cleaning with
awk '{if ($1 > 14) { print }}' common_buscos_ranked_by_counts.tbl | sed 's/\s\+/\t/g' | sed 's/^\t//g' | cut -f2 > filtered_buscos_by_counts.list
```
remember to add a "^" at the beginning of each id for easier extraction
```
%s/^/\^/g #this in vim is fine
```
the prepare a temporary table holding only important data
```bash
for file in `find . -maxdepth 1 -mindepth 1 -name "*.tsv"`
do
grep -E -f filtered_buscos_by_counts.list <(cut -f1,3,4,7 $file) > $file.tmp_table
done
```


Two simple python scripts were used to extract a json file of correspondence between busco id and best match in each transcriptome, this json file was used to extract such sequences from each transcriptome and save them as a fasta file in which sequences ids are replaced with corresponding busco id.  ( filter_table.py and extract_by_busco.py )

This creates a json of matching ids and their buscos for each temporary table
```bash
for file in `find . -maxdepth 1 -mindepth 1 -name "*.tsv.tmp_table"`
do
python filter_table.py $file                                                                                                        done
```



TransDecoder.LongOrfs and TransDecoder.Predict (with the option to keep only the best orf) were used to extract the coding sequences.

This awk line converts multi line fastas to single-line fastas
```bash
awk '/^>/ {printf("\n%s\n",$0); next;} { printf("%s",$0);} END {printf("\n")}'
```

This converts a grep output to a fasta, it is useful to split and merge files by busco id
```
grep -A 1 10163at7742 *selected_buscos.fasta | sed 's/\(^.\+:>\)/>\1/g' | sed 's/\..\+:>/:/g' | sed 's/^.\+-//g' | sed '/^--$/d' 
```
this is better
```bash
grep -A 1 -e "^>${id}" *selected_buscos.fasta | sed 's/\(^.\+:>\)/>\1/g' | sed 's/\..\+:>/:/g' | sed 's/^.\+-//g' | sed '/^--$/d' | sed '/^$/d' > seprated_buscos/$id.fasta
```

this is for extracting and renaming the sequences for each species:
```bash
for file in `find . -maxdepth 1 -mindepth 1 -name "*.cds"`                                 
do
fn=$(basename $file); species=$(awk -F. '{print $1}' <<< "$fn")
grep -A 1 -F -f ../filtered_buscos_by_counts_forfasta.list $file | sed "s/^>/>$species:/g; /^--$/d" > $species.overlapping_CDS.fasta
done
```



### Alignment
Alignment was perfromed with macse with standard settings and aminoacid and codon alignments were saved for each single busco. 
```bash
find . -maxdepth 1 -mindepth 1 -name ".fasta" | parallel -I% --max-args 1 macse -prog alignSequences -seq %
```
or alternatively, with XARGS:
```bash
find . -maxdepth 1 -mindepth 1 -name "*.fasta" | xargs -I SEQS -P 120 java -jar ~/Software/macse_v2.05.jar -prog alignSequences -seq SEQS > SEQS.alignment.log 
```
A super alignment was created by merging the alignments from all the species by concatenation. The absence of a sequece was marked with a string of "-" character. The script concatenate_alignments.py does this.

### Trimming of alignment
The trimming of uninformative regions was performed with BMG with the following command:
```bash
bmge -t CODON -of trimmed_seqs.fasta -m BLOSUM95 -i input_alignment.fasta -g 0
```
this first trimming was performed with very stringent parameters, as BLOSUM95 substitution matrix and 0% allowed gaps

### Subsetting
in order to use codeml we need to use the genes that are present in all the alignments, and to obtain useful information we set the length threshold for the trimmed alignment to 100 codons. 
```bash
for file in `find -maxdepth 1 -mindepth 1 -name "*NT.fasta.trimmed.phy"`
do
cp $(awk 'NR==1{if ($2 >= 300) print FILENAME; else print gatto}' $file) longer_than_100_codons/
done
```

### Tree models
the newick files for the trees are in the files folder.
#### one subgroup

first attempt was performed using only one subgroup for lungfishes.

#### two subgroup

second attempt is being performed using one subgroup for lungfishes and another for tetrapods

### codeml

codeml was used to evaluate all the separate aligned genes and a super-alignment created by concatenating all the trimmed sequences.

"X" characters were substituted by "-"

model was set to 0 for the unannotated tree and to 2 for the annotated tree. A sample config file for codeml follows:

```
seqfile = full_alignment.phy 
outfile = full_alignment_output_model_2
treefile = Phylogeny_corretta_alternativo.nwk
noisy = 9
verbose = 0
runmode = 0
seqtype = 1
CodonFreq = 2
* ndata = 10
clock = 0
aaDist = 0
aaRatefile = awg.dat
model = 2
NSsites = 0
icode = 0
Mgene = 0
fix_kappa = 0
kappa = 2
fix_omega = 0
omega = .4
fix_alpha = 1
alpha = 0
Malpha = 0
ncatG = 3
fix_rho = 1
rho = 0
getSE = 0
RateAncestor = 0
Small_Diff = .5e-6
* cleandata = 0
* fix_blength = 0
method = 0

```
5 different annotations for the tree were used:
+ no annotation at all form model 0
+ lungfish as a separate clade
+ lungfish as a separate clade, but with *Cynops orientalis* labelled like a lunghfish.
+ lungfish and tetrapoda ad two separate clades
+ lungfish and tetrapoda ad two separate clades, but with *Cynops orientalis* labelled like a lunghfish.

extracting data from the output of codeml and formatting it into tables. The join step needs to be modified accordingly to the number of columns in the different outputs.
```bash
# extraxt log likelihood for the two opposing models
grep lnL *.d/*.model_2 | sed 's/_NT.\+:\s\+/\t/g' | sed 's/\s/\t/g' | cut -f1,2 > lnL_model2.tsv
grep lnL *.d/*.model_0 | sed 's/_NT.\+:\s\+/\t/g' | sed 's/\s/\t/g' | cut -f1,2 > lnL_model0.tsv

# extract the dN/dS values for the two opposing models
grep "omega (dN/dS)" *.d/*.model_0 | sed 's/_NT.\+=\s\+/\t/g'> omega_model0.tsv
grep "w (dN/dS)" *.d/*.model_2 | sed 's/_NT.\+:\s\+/\t/g' | sed 's/\s/\t/g' > omega_model2.tsv

# merge results
join -j 1 -o 1.1 1.2 2.2 <(sort -k1 lnL_model0.tsv ) <(sort -k 1 lnL_model2.tsv) | sed 's/\s/\t/g' > lnl_merge.tsv
join -j 1 -o 1.1 1.2 2.2 2.3 2.4 2.5 <(sort -k1 omega_model0.tsv ) <(sort -k 1 omega_model2.tsv) | sed 's/\s/\t/g' > omega_merged.tsv
join -j 1 -o 1.1 1.2 1.3 2.2 2.3 2.4 2.5 <(sort -k1 lnl_merge.tsv ) <(sort -k 1 omega_merged.tsv) | sed 's/\s/\t/g' > all_merged.tsv

```
This creates a table with the following columns in order:
```
ortho_id        lnL_model0      lnL_model2      w_model0        w0      w_lungfish      w_other_vertebrata
```
the order of the last columns depends on the labelling of the reference tree.

The tables are available at the files folder.

### Gene Ontology and enrichment analysis
Gene Ontology terms relative to each busco id were retrieved from OrthoDB via its web API. The enrichment testing was performed by means of hypergeometric test with [this script](https://gitlab.com/54mu/enrichment_test) and thresholds were set to FDR < 0.05 and Observed-Expected > 2. The common ortholog list was used as "universe" and the relaxed and constrained datesets were used as subset; finally the total number of common ortholog was used as universe size (thus accounting for busco id with no GO annotation).

## Results
### Selection of candidate orthologs
A total of 2189 buscos are common to the 15 assemblies, but 87 were lost due to absence of CDS in at least one species, in the end 2102 ortholog genes were present into all 15 species.

1928 have at least 100 codons. This is an enhancement compared to the previous results (recoevred fragmented sequences?)

### Relationships of w and evolution rates
It appears there is direct proportionality between evolution rates from busco and w values for all models 

![20201201143237](img/20201201143237.png)

### Merged alignments
#### model 0
```
lnL(ntime: 21  np: 23): -17798452.778956
omega (dN/dS) =  0.10758
```
#### model 2
```
lnL(ntime: 21  np: 25): -17786059.179504
w (dN/dS) for branches:  0.10903 0.09421 0.15678
```
the difference is significative here, as the log ratio test has pvalue = 0

#### model 2.1 - OLD DATA -
With *Cynops* grouped with lungfishes (megagenome clade)
```
lnL(ntime: 26  np: 30): -16569312.379562
w (dN/dS) for branches:  0.32328 0.14165 0.09286
```
the difference is significative also here

#### models with labelled genera of lungfishes - OLD DATA -
*Lepidosiren paradoxa*
```
lnL(ntime: 26  np: 30): -16576598.530141
w (dN/dS) for branches:  0.46547 0.18580 0.10407
```

*Neoceratodus*
```
lnL(ntime: 26  np: 30): -16577480.069172
w (dN/dS) for branches:  0.44926 0.16840 0.10402
```

*Protopterus*
```
lnL(ntime: 26  np: 30): -16554739.604500
w (dN/dS) for branches:  0.40123 0.39838 0.10000
```

### Gene Ontology erichment  of BUSCOs under relaxed selection -OLD DATA-

Enriched GOs sorted by significativity (they all have pvalue < 0.05)

|GOID      |GO_description                                                                  |
|----------|--------------------------------------------------------------------------------|
| GO:0005689 |U12-type spliceosomal complex                                                   |
|GO:0016593|Cdc73/Paf1 complex                                                              |
|GO:0017124|SH3 domain binding                                                              |
|GO:0097431|mitotic spindle pole                                                            |
|GO:0075522|IRES-dependent viral translational initiation                                   |
|GO:0032007|negative regulation of TOR signaling                                            |
|GO:0006397|mRNA processing                                                                 |
|GO:0034644|cellular response to UV                                                         |
|GO:0005681|spliceosomal complex                                                            |
|GO:0003924|GTPase activity                                                                 |
|GO:0006468|protein phosphorylation                                                         |
|GO:0009411|response to UV                                                                  |
|GO:0007049|cell cycle                                                                      |
|GO:0030027|lamellipodium                                                                   |
|GO:0032039|integrator complex                                                              |
|GO:0035327|transcriptionally active chromatin                                              |
|GO:0031648|protein destabilization                                                         |
|GO:0032436|positive regulation of proteasomal ubiquitin-dependent protein catabolic process|
|GO:0016180|snRNA processing                                                                |
|GO:0045202|synapse                                                                         |
|GO:0015629|actin cytoskeleton                                                              |
|GO:0005765|lysosomal membrane                                                              |
|GO:0071013|catalytic step 2 spliceosome                                                    |
|GO:0019827|stem cell population maintenance                                                |
|GO:0045171|intercellular bridge                                                            |
|GO:0016567|protein ubiquitination                                                          |
|GO:0045111|intermediate filament cytoskeleton                                              |
|GO:1904874|positive regulation of telomerase RNA localization to Cajal body                |
|GO:0005911|cell-cell junction                                                              |
|GO:0019903|protein phosphatase binding                                                     |
|GO:0046777|protein autophosphorylation                                                     |
|GO:0010628|positive regulation of gene expression                                          |
|GO:0008285|negative regulation of cell population proliferation                            |
|GO:0050660|flavin adenine dinucleotide binding                                             |
|GO:0034198|cellular response to amino acid starvation                                      |
|GO:0008134|transcription factor binding                                                    |


Many GOs are related to splicing, genome stabilty and cell cycle regulation. 

## Transcriptional efficiency of *Neoceratodus forsteri*

It is possible that the transcriptional efficiency is lower in lungfish (more splicing errors). To test this exons and complete gene sequences are extracted from genomes by the gff annotations with the get_them_CDS.py script. 

Then  RNA-seq reads are mapped on both the sequece files and the coverages are compared. The following is a comparison between *Neoceratodus forsteri* and *Anguilla anguilla*. For a clearer view only genes with coverage > 1 in at least one of the exons or genes sets are shown.

<div align="center">

![20210223125413](img/20210223125413.png)

</div>

The intron coverage is calculated as:
```math
coverage_{introns} = \frac{Nreads_{genes} - Nreads_{exons}}{length_{genes} - length_{exons}}
```

### issues with the previous analysis
Due to my bad understanding of the manual of codeml i had to re-run the w analyses, with the tree labeled as follows:

lungfish in foreground tree:
```tex
(Callorhinchus_milii,((Lepisosteus_oculatus,(Anguilla_anguilla,Danio_rerio)),((Neoceratodus_forsteri,(Lepidosiren_paradoxa,(Protopterus_dolloi,(Protopterus_annectens,Protopterus_aethiopicus)))) $1,((Cynops_orientalis,Ambystoma_mexicanum),((Gallus_gallus,Anolis_carolinensis),(Homo_sapiens,Monodelphis_domestica))))));
```

tetrapods in foreground tree:
```tex
(Callorhinchus_milii,((Lepisosteus_oculatus,(Anguilla_anguilla,Danio_rerio)),((Neoceratodus_forsteri,(Lepidosiren_paradoxa,(Protopterus_dolloi,(Protopterus_annectens,Protopterus_aethiopicus)))),((Cynops_orientalis,Ambystoma_mexicanum),((Gallus_gallus,Anolis_carolinensis),(Homo_sapiens,Monodelphis_domestica))) $1)));
```
please remember that the labeling of trees in codeml follows the simple numerical order and the background does not need to be labelled, while the foreground needs to be labelled progressively starting from 1. 

## New results
the new results are very similar to the old ones, but a FDR correction was applied to the p-value in order to filter out false positives and the test was performed also with the tetrapoda branch as foreground.

```python
# code for FDR calculation
def fdr(pvalues):
    ranks = stats.rankdata(pvalues)
    fdr = pvalues * len(pvalues) / ranks
    fdr[fdr > 1 ] = 1
    return fdr
```

### relaxed selection in lungfish
Orthologs with FDR < 0.05 are 908, of these, 893 have an w higher in lungfish than the background. The distribution of the ratio between w in lungfish and in background looks like this:

![20210325132230](img/20210325132230.png)

using a log scale for the x axis:

![20210325132303](img/20210325132303.png)

Distribution of FDR corrected p-value:


![20210325132558](img/20210325132558.png)

#### Enrichment analysis of buscos under relaxed selections
The results are similar to the ones obtained previously and are reported in the files directory.

#### missing buscos common to all lungfish
the buscos which are missing in all the lungfish species analised are the following 50:
```tex
102147at7742
105242at7742
106301at7742
11104at7742
115007at7742
126705at7742
132224at7742
132656at7742
133713at7742
145938at7742
152020at7742
153242at7742
162664at7742
164538at7742
166380at7742
166801at7742
173102at7742
191412at7742
202098at7742
205403at7742
229422at7742
24468at7742
251511at7742
266951at7742
280835at7742
285589at7742
31418at7742
315832at7742
321789at7742
323076at7742
326283at7742
346436at7742
355312at7742
361842at7742
367625at7742
369463at7742
373540at7742
390186at7742
395792at7742
401448at7742
419468at7742
428749at7742
42971at7742
4372at7742
5604at7742
60870at7742
64333at7742
75522at7742
86277at7742
903at7742
```
Protein sequences of human corresponding to the ids in the list were downloaded with:
```bash
for ortho in `cat missing_list`
do
curl https://vdev.orthodb.org/fasta\?id\=${ortho}\&species\=9606 >> missing_buscos_Hs.fasta
done
```
and imported into CLC genomics workbench for blasting against lungfish genomes and transcriptomes and identify evident loss of function mutations. This analysis ended with finding only MBIP gene as incomplete in the N-terminal region in all the available lungfish genomes/transcriptomes.

An alignment of the translated protein sequences from *Neoceratodus forsteri* and *Protopterus annectens* with the ones from other vertebrates is reported below for reference.
![20210408115403](img/20210408115403.png)


### Relaxed selection in tetrapods
For comparison the same process was used by setting the tetrapoda branch as the foreground. 434 genes had a pvalue < 0.05, of which 158 resulted under relaxed selection in the foreground branch. 

Distribution of FDR corrected pvalues looks like this:

![20210325132750](img/20210325132750.png)

Distribution of significant $`\omega`$ ratios is (mind the simmetry! x axis is in log scale):

![20210325132932](img/20210325132932.png)

This is very different from the one from lungfish.

#### Enrichment test on relaxed genes in tetrapoda
Fewer GO terms were significant in this analisys and are reported here (observed - expected > 2)

```
GO:0031167	rRNA methylation
GO:0042645	mitochondrial nucleoid
GO:0005739	mitochondrion
GO:0045494	photoreceptor cell maintenance
GO:0032543	mitochondrial translation
GO:0001750	photoreceptor outer segment
GO:0019843	rRNA binding
GO:0000793	condensed chromosome
GO:0048487	beta-tubulin binding
GO:0007605	sensory perception of sound
GO:0032259	methylation
GO:0016021	integral component of membrane
GO:0016020	membrane
GO:0048471	perinuclear region of cytoplasm
```

## intron sizes and expression of orthologs
Greater values of $`\omega`$ are expected to correspond to greater intron sizes and smaller expression values.
The intron size distribution of the tested orthologs, if binned by quantiles of $`\omega`$ looks like this:

![20210325133522](img/20210325133522.png)

The significance of the difference between bins was tested with Kruskall-Wallis test and resulted sinificative when testing for any difference, and the pairwise tests results are reported in the image below. 

![20210325133554](img/20210325133554.png)

The same goes for the expression values. Here I considered the maximum expression value for each gene among all available tissues. 

![20210325133657](img/20210325133657.png)

And the relative significance scores:

![20210325133733](img/20210325133733.png)

The whiskers in the boxplot show the 95th percentile of the distribution.


## Comparison with Meyer's positively selected genes
The orthologs were compared to the genes which are reported in the paper by Meyer as being positively selected in lungfish, by searching them among the ones with relaxed selection and significative fdr corrected pvalue in our dataset. 15 were found overlapping with the model1.

<div align="center">
model 1 overlaps:

|gene_id		|Symbol	|busco_id	|fdr	 |
|-----------------------|-------|---------------|--------|
|asmbl_11921.P1		|SPATA2	|169465at7742	|0.000108|
|asmbl_18112.P1		|NOP16	|383347at7742	|0.001292|
|asmbl_50509.P1		|DDX42	|109869at7742	|0.001235|
|asmbl_57331.P1		|TRNT1	|209931at7742	|0.000608|
|asmbl_100653.P1	|ITIH2	|87513at7742	|0.045051|
|asmbl_107047.P1	|Trit1	|244028at7742	|0.00196 |
|asmbl_117437.P1	|PUM3	|133596at7742	|0.000244|
|asmbl_123949.P1	|Cstf3	|96554at7742	|0.000106|
|asmbl_129090.P1	|Taf1	|15604at7742	|0.022422|
|asmbl_167877.p1	|SLC30A7|318959at7742	|0.033933|
|asmbl_188582.P1	|Rb1cc1	|41154at7742	|0.001118|
|asmbl_192312.P1	|COG1	|101309at7742	|0.047116|
|asmbl_200016.P1	|CNOT10	|109081at7742	|0.001268|
|asmbl_205853.P1	|CASC4	|330809at7742	|1E-06   |
|asmbl_219823.P1	|DNAJC9	|393341at7742	|0.000743|

</div>


## Expression of transposable elements

### Detection of TE

RepeatMasker 4.4.1 with Dfam 3.2 as library.

The transcriptome was filterend to retain only the longest isoform per gene. 
```bash
RepeatMasker -pa 64 NeoceratodusV2.1.trinity.Trinity.fa
```

RepeatScout 1.0.6 was used as per [this protocol](https://openwetware.org/wiki/Wikiomics:Repeat_finding#RepeatScout) with the basic analysis

```bash
build_lmer_table -sequence NeoceratodusV2.1.trinity.Trinity.longest.fa.masked -freq output_lmer.frequency
RepeatScout -sequence NeoceratodusV2.1.trinity.Trinity.longest.fa.masked -output output_repeats.fas -freq output_lmer.frequency
filter-stage-1.prl output_repeats.fas > output_repeats.fas.filtered_1
RepeatMasker -pa 120 NeoceratodusV2.1.trinity.Trinity.longest.fa.masked -lib output_repeats.fas.filtered_1
cat output_repeats.fas.filtered_1| filter-stage-2.prl --cat=NeoceratodusV2.1.trinity.Trinity.longest.fa.masked.out > output_repeats.fas.filtered_2
```
This produces 3335 sequences.

### False positive removal

diamond blastx was used to remove valid hits of candidate repeats on proteomes form *Latimeria chalumnae* and *Xenopus tropicalis*. 

```bash
diamond blastx -p 120 -d Xenopus.dmnd -o repeat_vs_Xenopus.tsv -f 6 qseqid sseqid evalue --max-hsps 1 -k 1 -e 1e-5 -q ../output_repeats.fas.filtered_2
diamond blastx -p 120 -d Latimeria.dmnd -o repeat_vs_Latimeria.tsv -f 6 qseqid sseqid evalue --max-hsps 1 -k 1 -e 1e-5 -q ../output_repeats.fas.filtered_2
```
300 sequences were removed in this step.

only sequences longer than 100 nt were retained.
```python
with open("library_filtered_by_len.fasta", "w") as of:
    for seq in SeqIO.parse("almost_final_library.fasta", "fasta"):
        if len(seq.seq) > 100:
	    of.write(">{}\n{}\n".format(seq.id, seq.seq))
```
This removed 1326 sequences.

interproscan 5.34-73.0 was used to search for domains in the candidate repeats library
```bash
~/Software/interproscan-5.34-73.0/interproscan.sh -appl TIGRFAM,SFLD,SUPERFAMILY,ProSiteProfiles,SMART,PRINTS,ProSitePatterns,Pfam -i library_filtered_by_len.fasta  -iprlookup -b interproscan_run -t n -dra -cpu 120
```
194 sequences were removed in this step.

sequences with domains of **reverse transcriptase**, **integrase** or **transposase** were selected with
```bash
grep -e "\(\(R\|r\)everse \(T\|t\)ranscriptase\|\(I\|i\)ntegrase\|\(T\|t\)ransposase\)" interproscan_run.tsv | cut -f1 | sed 's/_.\+$//g' | sort | uniq
```
38 sequences were readded in this step.

A total of 1553 sequences reained in the library.

Finally coverage of the library was calculated by mapping in CLC with the following parameters:
```
  References = final_library
  Masking mode = No masking
  Match score = 1
  Mismatch cost = 2
  Cost of insertions and deletions = Linear gap cost
  Insertion cost = 3
  Deletion cost = 3
  Length fraction = 0.75
  Similarity fraction = 0.95
  Global alignment = No
  Auto-detect paired distances = Yes
  Non-specific match handling = Map randomly
  Output mode = Create stand-alone read mappings
  Create report = Yes
  Collect unmapped reads = No
```
and only sequences with average coverage > 10 were kept. The final library contains 605 sequences.

### Classification and quantification of TE families
classification was done with TEclass.
Expression values were evaluated for  the two individuals in the brain, liver, lung and testis tissues. 

Relative expression plots:

![20210415190522](img/20210415190522.png)

## Future

see issues

## Pervasive transcription

#### Download RNA-seq reads and genomes

  + [x] *Gadus morhua* SRP058865 
  + [x] *Salmo trutta* SRP045101
  + [x] *Anguilla anguilla* SRP045099
  + [x] *Perca fluviatilis* SRP045144
  + [x] *Thymallus thymallus* SRP045142 - genome [here](https://figshare.com/articles/dataset/Grayling_draft_genome_dataset/5135257?backTo=/collections/Grayling_draft_genome_assembly_and_analysis/3808162)
  + [x] *Oryzas latipes* SRP044784
  + [x] *Esox lucius* SRP045141
  + [x] *Pangasianodon hypophthalmus* SRP045140
  + [x] *Lepisosteus oculatus* SRP044782
  + [x] *Danio rerio*
  + [x] *Noceratodus forsteri*
  + [ ] *Protopterus annectens*
  + [ ] *Ambystoma mexicanum*

