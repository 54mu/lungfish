## this script takes a json file and a fasta and outputs sequences present in the fasta as 
## a fasta file replacing the id with the corresponding busco_id

from Bio.SeqIO import parse
import sys
import json

j = open(sys.argv[1], "r")
d = json.load(j)
j.close()

for seq in parse(sys.argv[2], "fasta"):
    if (seq.id not in d.keys()):
        pass
    else:
        print(">{}\n{}".format(d[seq.id], str(seq.seq)))

