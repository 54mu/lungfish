### script that filters the table from busco by only keeping the highest scoring pairs
import pandas as pd
import sys
import json

tbl = pd.read_csv(sys.argv[1], sep = "\t", names=["busco_id", "seq_id", "score", "description"])
tbl["maxscore"] = tbl.groupby(by = "busco_id")["score"].transform(max)
ntbl = tbl[tbl.score == tbl.maxscore].drop_duplicates("busco_id")
ntbl = ntbl[["busco_id", "seq_id"]].set_index("seq_id")

## extracting a dictionary from pairs, this will be useful in extracting sequences from transcriptomes

d = ntbl.to_dict()["busco_id"]

with open(sys.argv[1]+".pairs.json", "w") as json_output:
    json_output.write(json.dumps(d))


