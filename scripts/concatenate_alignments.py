## this script concatenates several alignments for a given list of species 
import sys

## loads all sequences in memory and builds a list of species and genes in order to be as flexible as possible. the only input is the list of alignments

species = set()
seqs = {}
seqmap = {}
for i in sys.argv[1:]:
    #print(i)
    with open(i, "r") as infile:
        latest_species = ""
        latest_seq = ""
        for line in infile.readlines():
            if line[0] == ">":
                latest_species = line.split(":")[0][1:]
                species.add(latest_species)
                latest_seq = line.split(":")[1][:-1]
                #seqs[latest_seq] = len(latest_seq)
            else:
                try:
                    seqmap[latest_species][latest_seq] = line.rstrip()
                    seqs[latest_seq] = len(line.rstrip())
                except:
                    seqmap[latest_species] = {latest_seq : line.rstrip()}
                    seqs[latest_seq] = len(line.rstrip())

for s in species:
    finalseq = ""
    for seq in sorted(seqs.keys()):
        try:
            finalseq = finalseq + seqmap[s][seq]
        except:
            finalseq = finalseq + ("-" * seqs[seq])
    print(">{}\n{}\n".format(s, finalseq))
